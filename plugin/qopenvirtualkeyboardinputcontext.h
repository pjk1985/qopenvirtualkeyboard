/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef QOPENVIRTUALKEYBOARDIMPUTCONTEXT_H
#define QOPENVIRTUALKEYBOARDIMPUTCONTEXT_H

#include <qpa/qplatforminputcontext.h>
#include <QPointer>

class KeyboardPanel;
class QOpenVirtualkeyboard;

class QOpenVirtualkeyboardImputContext : public QPlatformInputContext
{
    Q_OBJECT
public:
    QOpenVirtualkeyboardImputContext();
    ~QOpenVirtualkeyboardImputContext() override;

    void registerTypes(const char *uri);

    bool isValid() const override;

    void reset() override;
    void commit() override;
    void update(Qt::InputMethodQueries) override;
    void invokeAction(QInputMethod::Action, int cursorPosition) override;
    bool filterEvent(const QEvent *event) override;
    void showInputPanel() override;
    void hideInputPanel() override;

    QObject *focusObject() const;
    void setFocusObject(QObject *object) override;

    QOpenVirtualkeyboard *keyboard() const;

private:
    void setVirtualKeyboard(QOpenVirtualkeyboard *vk);

signals:
    void focusObjectChanged();

private:
    QScopedPointer<KeyboardPanel> m_panel;
    QPointer<QObject> m_focusObject;
    QPointer<QOpenVirtualkeyboard> m_keyboard;

    friend class QOpenVirtualkeyboard;
};

#endif // QOPENVIRTUALKEYBOARDIMPUTCONTEXT_H
