/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef QOPENVIRTUALKEYBOARD_H
#define QOPENVIRTUALKEYBOARD_H

#include <QObject>
#include <QRect>

class QOpenVirtualkeyboardPrivate;

class QOpenVirtualkeyboard : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QOpenVirtualkeyboard)
    Q_PROPERTY(InputMode inputMode READ inputMode WRITE setInputMode NOTIFY inputModeChanged)
    Q_PROPERTY(QRectF keyboardRectangle READ keyboardRectangle WRITE setKeyboardRectangle NOTIFY keyboardRectangleChanged)
    Q_PROPERTY(bool animating READ isAnimating WRITE setAnimating NOTIFY animatingChanged)
public:
    enum class InputMode {
        Latin,
        Numeric,
        Hangul
    };
    Q_ENUM(InputMode)

    explicit QOpenVirtualkeyboard(QObject *parent = nullptr);
    ~QOpenVirtualkeyboard();

    InputMode inputMode();
    void setInputMode(const InputMode &mode);

    QRectF keyboardRectangle();
    void setKeyboardRectangle(const QRectF &rectangle);
    bool isAnimating();
    void setAnimating(bool isAnimating);

    Q_INVOKABLE void show();
    Q_INVOKABLE void hide();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void commit();
    Q_INVOKABLE void clickKey(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers);

private:

signals:
    void inputModeChanged();
    void keyboardRectangleChanged();
    void animatingChanged();

public slots:


private:
    QScopedPointer<QOpenVirtualkeyboardPrivate> d_ptr;
};

#endif // QOPENVIRTUALKEYBOARD_H
