/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import QtQuick.QOpenVirtualkeyboard.Plugins 1.0

LayoutBase {
    inputMode: QOpenVirtualkeyboard.Hangul

    keyModel: KeyModel{
        customFirstRowModel: ListModel{
            ListElement{ text1: "\u3142"; text2 : "\u3143" }
            ListElement{ text1: "\u3148"; text2 : "\u3149" }
            ListElement{ text1: "\u3137"; text2 : "\u3138" }
            ListElement{ text1: "\u3131"; text2 : "\u3132" }
            ListElement{ text1: "\u3145"; text2 : "\u3146" }
            ListElement{ text1: "\u315B"; text2 : "\u315B" }
            ListElement{ text1: "\u3155"; text2 : "\u3155" }
            ListElement{ text1: "\u3151"; text2 : "\u3151" }
            ListElement{ text1: "\u3150"; text2 : "\u3152" }
            ListElement{ text1: "\u3154"; text2 : "\u3156" }
        }

        customSecondRowModel: ListModel{
            ListElement{ text1: "\u3141"; text2 : "\u3141" }
            ListElement{ text1: "\u3134"; text2 : "\u3134" }
            ListElement{ text1: "\u3147"; text2 : "\u3147" }
            ListElement{ text1: "\u3139"; text2 : "\u3139" }
            ListElement{ text1: "\u314E"; text2 : "\u314E" }
            ListElement{ text1: "\u3157"; text2 : "\u3157" }
            ListElement{ text1: "\u3153"; text2 : "\u3153" }
            ListElement{ text1: "\u314F"; text2 : "\u314F" }
            ListElement{ text1: "\u3163"; text2 : "\u3163" }
        }

        customThirdRowModel: ListModel{
            ListElement{ text1: "\u314B"; text2 : "\u314B" }
            ListElement{ text1: "\u314C"; text2 : "\u314C" }
            ListElement{ text1: "\u314A"; text2 : "\u314A" }
            ListElement{ text1: "\u314D"; text2 : "\u314D" }
            ListElement{ text1: "\u3160"; text2 : "\u3160" }
            ListElement{ text1: "\u315C"; text2 : "\u315C" }
            ListElement{ text1: "\u3161"; text2 : "\u3161" }
        }
    }

    sourceComponent: hangulLayout

    Component{
        id: hangulLayout
        Column {
            id: column
            anchors.margins: 5
            anchors.fill: parent
            spacing: pimpl.verticalSpacing

            QtObject {
                id: pimpl
                property bool shiftModifier: false
                property bool symbolModifier: false
                property int verticalSpacing: 10
                property int horizontalSpacing: 5
                property int rowHeight: keyboard.height / 4 - verticalSpacing
                property int buttonWidth: (keyboard.width - column.anchors.margins) / 10 - horizontalSpacing
                property color textColor: "white"
            }

            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater {
                    model: keyModel.customFirstRowModel
                    delegate: Component {
                        Key {
                            width: pimpl.buttonWidth
                            height: pimpl.rowHeight
                            text: (pimpl.shiftModifier) ? text2 : (pimpl.symbolModifier) ? keyModel.firstRowModel.get(index).firstSymbol : text1
                            textColor: pimpl.textColor
                            onClicked: {
                                var element = keyModel.firstRowModel.get(index)
                                var code = (pimpl.shiftModifier) ? element.letter.toUpperCase() : (pimpl.symbolModifier) ? element.firstSymbol : element.letter
                                QOpenVirtualkeyboard.clickKey(0, code, 0)
                            }
                        }
                    }
                }
            }
            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater {
                    model: keyModel.customSecondRowModel
                    delegate: Component {
                        Key {
                            width: pimpl.buttonWidth
                            height: pimpl.rowHeight
                            text: (pimpl.shiftModifier) ? text2 : (pimpl.symbolModifier) ? keyModel.secondRowModel.get(index).firstSymbol : text1
                            textColor: pimpl.textColor
                            onClicked: {
                                var element = keyModel.secondRowModel.get(index)
                                var code = (pimpl.shiftModifier) ? element.letter.toUpperCase() : (pimpl.symbolModifier) ? element.firstSymbol : element.letter
                                QOpenVirtualkeyboard.clickKey(0, code, 0)
                            }
                        }
                    }
                }
            }
            Item {
                height: pimpl.rowHeight
                width: parent.width
                Key {
                    anchors.left: parent.left
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    textColor: (pimpl.shiftModifier)? "yellow" : pimpl.textColor
                    text: "\uf0aa"
                    textSize: 60
                    onClicked: {
                        if (pimpl.symbolModifier) {
                            pimpl.symbolModifier = false
                        }
                        pimpl.shiftModifier = !pimpl.shiftModifier
                    }
                }

                Row {
                    height: pimpl.rowHeight
                    spacing: pimpl.horizontalSpacing
                    anchors.horizontalCenter: parent.horizontalCenter
                    Repeater {
                        anchors.horizontalCenter: parent.horizontalCenter
                        model: keyModel.customThirdRowModel
                        delegate: Component {
                            Key {
                                width: pimpl.buttonWidth
                                height: pimpl.rowHeight
                                text: (pimpl.shiftModifier) ? text2 : (pimpl.symbolModifier) ? keyModel.thirdRowModel.get(index).firstSymbol : text1
                                textColor: pimpl.textColor
                                onClicked: {
                                    var element = keyModel.thirdRowModel.get(index)
                                    var code = (pimpl.shiftModifier) ? element.letter.toUpperCase() : (pimpl.symbolModifier) ? element.firstSymbol : element.letter
                                    QOpenVirtualkeyboard.clickKey(0, code, 0)
                                }
                            }
                        }
                    }
                }

                Key {
                    anchors.right: parent.right
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    text: "\uf060"
                    textSize: 60
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Backspace, "", 0)
                }
            }

            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: ""
                    text: ""

                    Image{
                        anchors.centerIn: parent
                        sourceSize.width: 256 * 0.2
                        sourceSize.height: 256 * 0.2
                        source: "qrc:/images/hide-keyboard-button.svg"
                    }

                    onClicked: {
                        Qt.inputMethod.hide()
                    }
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    text: ""//"\uf0ac"
                    textSize: 60
                    textColor: pimpl.textColor

                    Image{
                        anchors.centerIn: parent
                        sourceSize.width: 256 * 0.2
                        sourceSize.height: 256 * 0.2
                        source: "qrc:/images/worlwide.svg"
                    }
                    onClicked: {
                        QOpenVirtualkeyboard.inputMode = QOpenVirtualkeyboard.Latin
                    }
                }
                Key {
                    width: pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: ","
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Period, text, 0)
                }
                Key {
                    width: 3 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: " "
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Space, text, 0)
                }
                Key {
                    width: pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: "."
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Comma, text, 0)
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: (!pimpl.symbolModifier)? "12#" : "ABC"
                    textColor: pimpl.textColor
                    onClicked: {
                        if (pimpl.shiftModifier) {
                            pimpl.shiftModifier = false
                        }
                        pimpl.symbolModifier = !pimpl.symbolModifier
                    }
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: "Enter"
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Return, "", 0)
                }
            }
        }
    }
}
