/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <qpa/qplatforminputcontextplugin_p.h>

#include "qopenvirtualkeyboardinputcontext.h"
#include "plugin.h"

static const char pluginsUri[] = "QtQuick.QOpenVirtualkeyboard.Plugins";
static const char inputMethodEnvVariable[] = "QT_IM_MODULE";

QPlatformInputContext *QOpenVirtualkeyboardPlugin::create(const QString &system, const QStringList &paramList)
{
    Q_UNUSED(paramList)

    if (!qEnvironmentVariableIsSet(inputMethodEnvVariable))
        return nullptr;

    if (system == QLatin1String(PLUGINNAME)) {
        auto context = new QOpenVirtualkeyboardImputContext;
        context->registerTypes(pluginsUri);
        return context;
    }

    return nullptr;
}
