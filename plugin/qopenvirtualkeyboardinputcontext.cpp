/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QGuiApplication>
#include <QQuickView>

#include "qopenvirtualkeyboard.h"
#include "qopenvirtualkeyboardinputcontext.h"
#include "keyboardpanel.h"

QOpenVirtualkeyboardImputContext::QOpenVirtualkeyboardImputContext()
    : QPlatformInputContext(),
      m_panel(nullptr),
      m_focusObject(nullptr),
      m_keyboard(nullptr)
{

}

QOpenVirtualkeyboardImputContext::~QOpenVirtualkeyboardImputContext()
{

}

void QOpenVirtualkeyboardImputContext::registerTypes(const char *uri)
{
    // registertypes
    static auto callback = [](QQmlEngine *engine, QJSEngine *scriptEngine)->QObject *{
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return new QOpenVirtualkeyboard;
    };

    qmlRegisterSingletonType<QOpenVirtualkeyboard>(uri, 1, 0, "QOpenVirtualkeyboard", callback);
}

bool QOpenVirtualkeyboardImputContext::isValid() const
{
    return true;
}

void QOpenVirtualkeyboardImputContext::reset()
{
    if(m_keyboard){
        m_keyboard->reset();
    }
}

void QOpenVirtualkeyboardImputContext::commit()
{
    if(m_keyboard){
        m_keyboard->commit();
    }
}

void QOpenVirtualkeyboardImputContext::update(Qt::InputMethodQueries)
{
    if(m_panel == nullptr){
        m_panel.reset(new KeyboardPanel(this));
        m_panel->createView();
    }
}

void QOpenVirtualkeyboardImputContext::invokeAction(QInputMethod::Action act, int cursorPosition)
{
    Q_UNUSED(act)
    Q_UNUSED(cursorPosition)
}

bool QOpenVirtualkeyboardImputContext::filterEvent(const QEvent *event)
{
    auto type = event->type();
    if(type == QEvent::Type::KeyPress){
        if(m_keyboard){
            m_keyboard->commit();
        }
    }
    return false;
}

void QOpenVirtualkeyboardImputContext::showInputPanel()
{
    if(m_panel){
        m_panel->show();
    }
}

void QOpenVirtualkeyboardImputContext::hideInputPanel()
{
    if(m_panel){
        m_panel->hide();
    }
}

QObject *QOpenVirtualkeyboardImputContext::focusObject() const
{
    return m_focusObject;
}

void QOpenVirtualkeyboardImputContext::setFocusObject(QObject *object)
{
    m_focusObject = object;
}

QOpenVirtualkeyboard *QOpenVirtualkeyboardImputContext::keyboard() const
{
    return m_keyboard;
}

void QOpenVirtualkeyboardImputContext::setVirtualKeyboard(QOpenVirtualkeyboard *vk)
{
    if(vk){
        if(m_keyboard)
            disconnect(SIGNAL(keyboardRectangleChanged()));

        m_keyboard = vk;

        connect(m_keyboard, &QOpenVirtualkeyboard::keyboardRectangleChanged, [&](){
            if(m_panel){
                m_panel->updatePanelRect(m_keyboard->keyboardRectangle().toRect());
            }
        });
    }
}
