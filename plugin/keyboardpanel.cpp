/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QGuiApplication>
#include <QScreen>
#include <QQuickView>
#include <QRect>

#include "keyboardpanel.h"
#include "qopenvirtualkeyboardinputcontext.h"
#include "qopenvirtualkeyboard.h"

KeyboardPanel::KeyboardPanel(QObject *parent)
    : QObject(parent),
      m_view(nullptr)
{
    QScreen *screen = QGuiApplication::primaryScreen();
    connect(screen, SIGNAL(virtualGeometryChanged(QRect)), SLOT(repositionView(QRect)));
}

KeyboardPanel::~KeyboardPanel()
{

}

void KeyboardPanel::createView()
{
    if(m_view == nullptr){
        m_view.reset(new QQuickView());
        m_view->setColor(Qt::transparent);
        m_view->setFlags(m_view->flags() | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
        m_view->setFlags(m_view->flags() | Qt::Window | Qt::BypassWindowManagerHint);
        m_view->setSource(QUrl("qrc:/Keyboard.qml"));
        repositionView(QGuiApplication::primaryScreen()->availableGeometry());
        if (qGuiApp){
            connect(qGuiApp, &QGuiApplication::aboutToQuit, [&](){ if(m_view){ m_view.reset();} });
            connect(qGuiApp, &QGuiApplication::focusWindowChanged, [&](QWindow* w){
                disconnect(this, SLOT(focusWindowVisibleChanged(bool)));
                if (w)
                    connect(w, SIGNAL(visibleChanged(bool)), this, SLOT(focusWindowVisibleChanged(bool)));
            });
        }
    }
}

void KeyboardPanel::show()
{
    if(m_view){
        m_view->show();
    }
}

void KeyboardPanel::hide()
{
    if(m_view){
        m_view->hide();
    }
}

void KeyboardPanel::updatePanelRect(const QRect &rect)
{
    if(m_view){
        QRegion inputRegion(rect);
        m_view->setMask(inputRegion);
    }
}

void KeyboardPanel::repositionView(const QRect &rect)
{
    if(m_view.isNull())
        return;

    if(m_view->geometry() != rect){
        auto keyboard = qobject_cast<QOpenVirtualkeyboardImputContext*>(parent())->keyboard();
        keyboard->setAnimating(true);
        m_view->setResizeMode(QQuickView::SizeViewToRootObject);
        m_view->setGeometry(rect);
        m_view->setResizeMode(QQuickView::SizeRootObjectToView);
        keyboard->setAnimating(false);
    }
}

void KeyboardPanel::focusWindowVisibleChanged(bool visible)
{
    if(!visible){
        this->hide();
    }
}
