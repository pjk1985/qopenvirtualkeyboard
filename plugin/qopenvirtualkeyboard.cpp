/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QtGui/qpa/qplatformintegration.h>
#include <QtGui/private/qguiapplication_p.h>

#include "qopenvirtualkeyboardinputcontext.h"
#include "qopenvirtualkeyboard.h"

#include "hangulinputmethod.h"
#include "defaultinputmethod.h"

#pragma pack(1)
class QOpenVirtualkeyboardPrivate
{
    Q_DECLARE_PUBLIC(QOpenVirtualkeyboard)
public:
    QOpenVirtualkeyboardPrivate(QOpenVirtualkeyboard *q_ptr)
        : q_ptr(q_ptr),
          platformInputContext(nullptr),
          keyboardRect(),
          inputMode(QOpenVirtualkeyboard::InputMode::Hangul),
          animating(false)
    {

    }
    ~QOpenVirtualkeyboardPrivate(){
        qDeleteAll(inputmethods);
    }

    QOpenVirtualkeyboard *q_ptr;
    QOpenVirtualkeyboardImputContext *platformInputContext;
    QMap<QOpenVirtualkeyboard::InputMode, QPointer<InputMethodBase>> inputmethods;
    QRectF keyboardRect;
    QOpenVirtualkeyboard::InputMode inputMode;
    bool animating;
};
#pragma pack()

QOpenVirtualkeyboard::QOpenVirtualkeyboard(QObject *parent)
    : QObject(parent),
      d_ptr(new QOpenVirtualkeyboardPrivate(this))
{
    Q_D(QOpenVirtualkeyboard);
    auto platformIntegration = QGuiApplicationPrivate::instance()->platformIntegration();
    QPlatformInputContext *platformInputContext = platformIntegration->inputContext();
    d->platformInputContext = qobject_cast<QOpenVirtualkeyboardImputContext *>(platformInputContext);

    // TODO: load submodule plugin when runtime.
    d->inputmethods.insert(QOpenVirtualkeyboard::InputMode::Latin, new DefaultInputMethod);
    d->inputmethods.value(QOpenVirtualkeyboard::InputMode::Latin)->init();
    d->inputmethods.insert(QOpenVirtualkeyboard::InputMode::Hangul, new HangulInputMethod);
    d->inputmethods.value(QOpenVirtualkeyboard::InputMode::Hangul)->init();

    d->platformInputContext->setVirtualKeyboard(this);
}

QOpenVirtualkeyboard::~QOpenVirtualkeyboard()
{

}

QOpenVirtualkeyboard::InputMode QOpenVirtualkeyboard::inputMode()
{
    Q_D(QOpenVirtualkeyboard);

    return d->inputMode;
}

void QOpenVirtualkeyboard::setInputMode(const QOpenVirtualkeyboard::InputMode &mode)
{
    Q_D(QOpenVirtualkeyboard);

    if(d->inputMode != mode){
        auto inputMehtod = d->inputmethods.value(inputMode());

        if(inputMehtod){
            inputMehtod->update(d->platformInputContext->focusObject());
        }else{
            return;
        }
        d->inputMode = mode;
        emit inputModeChanged();
    }
}

QRectF QOpenVirtualkeyboard::keyboardRectangle()
{
    Q_D(QOpenVirtualkeyboard);

    return d->keyboardRect;
}

void QOpenVirtualkeyboard::setKeyboardRectangle(const QRectF &rectangle)
{
    Q_D(QOpenVirtualkeyboard);

    if(d->keyboardRect != rectangle){
        d->keyboardRect = rectangle;
        emit keyboardRectangleChanged();
    }
}

bool QOpenVirtualkeyboard::isAnimating()
{
    Q_D(QOpenVirtualkeyboard);

    return d->animating;
}

void QOpenVirtualkeyboard::setAnimating(bool isAnimating)
{
    Q_D(QOpenVirtualkeyboard);

    if(d->animating != isAnimating){
        d->animating = isAnimating;
        emit animatingChanged();
    }
}

void QOpenVirtualkeyboard::show()
{

}

void QOpenVirtualkeyboard::hide()
{

}

void QOpenVirtualkeyboard::reset()
{

}

void QOpenVirtualkeyboard::commit()
{
    Q_D(QOpenVirtualkeyboard);

    auto inputMehtod = d->inputmethods.value(inputMode());

    if(inputMehtod){
        inputMehtod->update(d->platformInputContext->focusObject());
    }
}

void QOpenVirtualkeyboard::clickKey(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers)
{
    Q_D(QOpenVirtualkeyboard);

    auto inputMehtod = d->inputmethods.value(inputMode());

    // compose
    if(inputMehtod){
        inputMehtod->keyEventHandler(d->platformInputContext->focusObject(), key, text, modifiers);
    }
}
