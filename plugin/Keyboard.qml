/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.QOpenVirtualkeyboard.Plugins 1.0
import "."

Item {
    id: root
    width: Screen.width
    height: keyboard.height

    property int defaultHeight: 480
    readonly property int inputMode: QOpenVirtualkeyboard.inputMode
    readonly property bool isRootItem: root.parent != null && root.parent.parent == null

    Binding {
        target: QOpenVirtualkeyboard
        property: "keyboardRectangle"
        value: mapToItem(null,
                         keyboard.x,
                         keyboard.y,
                         keyboard.width,
                         keyboard.height)

        when: !QOpenVirtualkeyboard.animating
    }

    FontLoader {
        source: "qrc:/FontAwesome.otf"
    }

    Rectangle {
        id: keyboard
        color: "black"
        width: parent.width
        height: defaultHeight
        anchors.bottom: parent.bottom

        MouseArea {
            anchors.fill: parent
        }

        Loader{
            id: layout
            anchors.fill: parent
            source: inputMode == QOpenVirtualkeyboard.Latin ? "LayoutDefault.qml" : "LayoutHangul.qml"
        }
    }
}
