/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0

Item {
    property ListModel customFirstRowModel: null
    property ListModel customSecondRowModel: null
    property ListModel customThirdRowModel: null

    property alias firstRowModel: first
    property alias secondRowModel: second
    property alias thirdRowModel: third

    ListModel {
        id:first
        ListElement { letter: "q"; firstSymbol: "1"}
        ListElement { letter: "w"; firstSymbol: "2"}
        ListElement { letter: "e"; firstSymbol: "3"}
        ListElement { letter: "r"; firstSymbol: "4"}
        ListElement { letter: "t"; firstSymbol: "5"}
        ListElement { letter: "y"; firstSymbol: "6"}
        ListElement { letter: "u"; firstSymbol: "7"}
        ListElement { letter: "i"; firstSymbol: "8"}
        ListElement { letter: "o"; firstSymbol: "9"}
        ListElement { letter: "p"; firstSymbol: "0"}
    }
    ListModel {
        id:second
        ListElement { letter: "a"; firstSymbol: "!"}
        ListElement { letter: "s"; firstSymbol: "@"}
        ListElement { letter: "d"; firstSymbol: "#"}
        ListElement { letter: "f"; firstSymbol: "$"}
        ListElement { letter: "g"; firstSymbol: "%"}
        ListElement { letter: "h"; firstSymbol: "&"}
        ListElement { letter: "j"; firstSymbol: "*"}
        ListElement { letter: "k"; firstSymbol: "?"}
        ListElement { letter: "l"; firstSymbol: "/"}
    }
    ListModel {
        id:third
        ListElement { letter: "z"; firstSymbol: "_"}
        ListElement { letter: "x"; firstSymbol: "\""}
        ListElement { letter: "c"; firstSymbol: "'"}
        ListElement { letter: "v"; firstSymbol: "("}
        ListElement { letter: "b"; firstSymbol: ")"}
        ListElement { letter: "n"; firstSymbol: "-"}
        ListElement { letter: "m"; firstSymbol: "+"}
    }
}
