TARGET = qopenvirtualkeyboardplugin

QT += qml quick gui-private

DEFINES += PLUGINNAME=\\\"qopenvk\\\"

include(../submodule/submodule.pro)

SOURCES += \
    $$PWD/qopenvirtualkeyboardinputcontext.cpp \
    $$PWD/plugin.cpp \
    $$PWD/inputmethodbase.cpp \
    $$PWD/qopenvirtualkeyboard.cpp \
    $$PWD/keyboardpanel.cpp

HEADERS += \
    $$PWD/qopenvirtualkeyboardinputcontext.h \
    $$PWD/plugin.h \
    $$PWD/inputmethodbase.h \
    $$PWD/qopenvirtualkeyboard.h \
    $$PWD/keyboardpanel.h

OTHER_FILES += \
    $$PWD/qopenvk.json \
    $$PWD/../LICENSE \
    $$PWD/../README.md

RESOURCES += \
    qopenvk.qrc

PLUGIN_TYPE = platforminputcontexts
PLUGIN_EXTENDS = -
PLUGIN_CLASS_NAME = QOpenVirtualKeyboardPlugin
load(qt_plugin)
