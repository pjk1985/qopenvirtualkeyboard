/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import QtQuick.QOpenVirtualkeyboard.Plugins 1.0

LayoutBase {
    inputMode: QOpenVirtualkeyboard.Latin
    keyModel: KeyModel{
        // use default KeyModel
    }
    sourceComponent: defaultLayout

    Component{
        id: defaultLayout

        Column {
            id: column
            anchors.margins: 5
            anchors.fill: parent
            spacing: pimpl.verticalSpacing

            QtObject {
                id: pimpl
                property bool shiftModifier: false
                property bool symbolModifier: false
                property int verticalSpacing: 10
                property int horizontalSpacing: 5
                property int rowHeight: keyboard.height / 4 - verticalSpacing
                property int buttonWidth: (keyboard.width - column.anchors.margins) / 10 - horizontalSpacing
                property color textColor: "white"
            }

            Component {
                id: keyDelegate
                Key {
                    width: pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: (pimpl.shiftModifier) ? letter.toUpperCase() : (pimpl.symbolModifier) ? firstSymbol : letter
                    textColor: pimpl.textColor
                    onClicked: {
                        QOpenVirtualkeyboard.clickKey(0, text, 0)
                    }
                }
            }

            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater {
                    model: keyModel.firstRowModel
                    delegate: keyDelegate
                }
            }
            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Repeater {
                    model: keyModel.secondRowModel
                    delegate: keyDelegate
                }
            }
            Item {
                height: pimpl.rowHeight
                width: parent.width
                Key {
                    anchors.left: parent.left
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    textColor: (pimpl.shiftModifier)? "yellow": pimpl.textColor
                    text: "\uf0aa"
                    textSize: 60
                    onClicked: {
                        if (pimpl.symbolModifier) {
                            pimpl.symbolModifier = false
                        }
                        pimpl.shiftModifier = !pimpl.shiftModifier
                    }
                }

                Row {
                    height: pimpl.rowHeight
                    spacing: pimpl.horizontalSpacing
                    anchors.horizontalCenter: parent.horizontalCenter
                    Repeater {
                        anchors.horizontalCenter: parent.horizontalCenter
                        model: keyModel.thirdRowModel
                        delegate: keyDelegate
                    }
                }

                Key {
                    anchors.right: parent.right
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    text: "\uf060"
                    textSize: 60
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Backspace, "", 0)
                }
            }

            Row {
                height: pimpl.rowHeight
                spacing: pimpl.horizontalSpacing
                anchors.horizontalCenter: parent.horizontalCenter
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: ""
                    text: ""

                    Image{
                        anchors.centerIn: parent
                        sourceSize.width: 256 * 0.2
                        sourceSize.height: 256 * 0.2
                        source: "qrc:/images/hide-keyboard-button.svg"
                    }

                    onClicked: {
                        Qt.inputMethod.hide()
                    }
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    font.family: "FontAwesome"
                    text: ""//"\uf0ac"
                    textSize: 60
                    textColor: pimpl.textColor

                    Image{
                        anchors.centerIn: parent
                        sourceSize.width: 256 * 0.2
                        sourceSize.height: 256 * 0.2
                        source: "qrc:/images/worlwide.svg"
                    }
                    onClicked: {
                        QOpenVirtualkeyboard.inputMode = QOpenVirtualkeyboard.Hangul
                    }
                }
                Key {
                    width: pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: ","
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Period, text, 0)
                }
                Key {
                    width: 3 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: " "
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Space, text, 0)
                }
                Key {
                    width: pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: "."
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Comma, text, 0)
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: (!pimpl.symbolModifier)? "12#" : "ABC"
                    textColor: pimpl.textColor
                    onClicked: {
                        if (pimpl.shiftModifier) {
                            pimpl.shiftModifier = false
                        }
                        pimpl.symbolModifier = !pimpl.symbolModifier
                    }
                }
                Key {
                    width: 1.25 * pimpl.buttonWidth
                    height: pimpl.rowHeight
                    text: "Enter"
                    textColor: pimpl.textColor
                    onClicked: QOpenVirtualkeyboard.clickKey(Qt.Key_Return, "", 0)
                }
            }
        }
    }
}
