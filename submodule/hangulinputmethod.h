/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef HANGULINPUTMETHOD_H
#define HANGULINPUTMETHOD_H

#include "inputmethodbase.h"

class HangulInputMethodPrivate;

class HangulInputMethod : public InputMethodBase
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(HangulInputMethod)
public:
    explicit HangulInputMethod(QObject *parent = nullptr);
    virtual ~HangulInputMethod() override;
    virtual void init() override;
    virtual void update(QObject *receiver) override;
    virtual void keyEventHandler(QObject *receiver, Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers) override;

    virtual QString reset();
    virtual bool compose(const QChar &str);
    virtual bool decompose();
    virtual QString commitString() const;
    virtual QString preeditString() const;

private:
    QScopedPointer<HangulInputMethodPrivate> d_ptr;
};

#endif // HANGULINPUTMETHOD_H
