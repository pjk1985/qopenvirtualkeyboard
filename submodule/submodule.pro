include(libhangul/libhangul.pri)

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/hangulinputmethod.h \
    $$PWD/defaultinputmethod.h

SOURCES += \
    $$PWD/hangulinputmethod.cpp \
    $$PWD/defaultinputmethod.cpp
