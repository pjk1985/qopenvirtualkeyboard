/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DEFAULTINPUTMETHOD_H
#define DEFAULTINPUTMETHOD_H

#include "inputmethodbase.h"

class DefaultInputMethod : public InputMethodBase
{
    Q_OBJECT
public:
    explicit DefaultInputMethod(QObject *parent = nullptr);
    virtual ~DefaultInputMethod() override;
    virtual void init() override;
    virtual void update(QObject *receiver) override;
    virtual void keyEventHandler(QObject *receiver, Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers) override;

signals:

public slots:
};

#endif // DEFAULTINPUTMETHOD_H
