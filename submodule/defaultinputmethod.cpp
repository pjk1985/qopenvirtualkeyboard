/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QGuiApplication>
#include <QInputMethodEvent>

#include "defaultinputmethod.h"

inline bool keyEventBypass(QObject *receiver,
                           QEvent::Type type,
                           Qt::Key key,
                           Qt::KeyboardModifier modifiers,
                           const QString& text = QString())
{
    QKeyEvent inputEvent(type, key, modifiers, text);
    return QGuiApplication::sendEvent(receiver, &inputEvent);
}

DefaultInputMethod::DefaultInputMethod(QObject *parent)
    : InputMethodBase(parent)
{

}

DefaultInputMethod::~DefaultInputMethod()
{

}

void DefaultInputMethod::init()
{

}

void DefaultInputMethod::update(QObject *receiver)
{
    Q_UNUSED(receiver)

    QList<QInputMethodEvent::Attribute> attributes;
    QInputMethodEvent inputEvent("", attributes);
    QGuiApplication::sendEvent(receiver, &inputEvent);
}

void DefaultInputMethod::keyEventHandler(QObject *receiver, Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers)
{
    Q_UNUSED(key)
    Q_UNUSED(modifiers)
    if(receiver == nullptr)
        return;

    switch (key) {
    case Qt::Key_Backspace:
    case Qt::Key_Return:
    case Qt::Key_Space:
        keyEventBypass(receiver, QEvent::KeyPress, key, Qt::NoModifier, text);
        return;
    default:
        QInputMethodEvent inputEvent;
        inputEvent.setCommitString(text);
        QGuiApplication::sendEvent(receiver, &inputEvent);
    }
}
