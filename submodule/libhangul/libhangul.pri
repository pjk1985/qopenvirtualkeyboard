INCLUDEPATH += $$PWD/hangul

# The following define makes your compiler warn you if you use any
# feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += \
        QT_DEPRECATED_WARNINGS \
        LIBHANGUL_DEFAULT_HANJA_DIC=\\\"$$PWD/data/hanja/hanja.txt\\\" \
        LIBHANGUL_DATA_DIR=\\\"$$PWD/data\\\"


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Input
HEADERS += \
        $$PWD/hangul/hangul-gettext.h \
        $$PWD/hangul/hangul.h \
        $$PWD/hangul/hangulinternals.h \
        $$PWD/hangul/hangulkeyboard.h \
        $$PWD/hangul/hanjacompatible.h
SOURCES += \
        $$PWD/hangul/hangulctype.c \
        $$PWD/hangul/hangulinputcontext.c \
        $$PWD/hangul/hangulkeyboard.c \
        $$PWD/hangul/hanja.c \

OTHER_FILES += \
        $$PWD/data/hanja/hanja.txt
