/*
 * QOpenVirtualkeyboard
 *
 * Copyright (C) 2019 PARK JONG KYU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QGuiApplication>
#include <QInputMethodEvent>

#include "hangul.h"
#include "hangulinputmethod.h"

#ifdef __cplusplus
extern "C" {
#endif

class HangulInputMethodPrivate
{
    Q_DECLARE_PUBLIC(HangulInputMethod)
public:
    HangulInputMethodPrivate(HangulInputMethod * q_ptr)
        : q_ptr(q_ptr),
          hic(nullptr){

    }

    ~HangulInputMethodPrivate(){
        q_ptr = nullptr;
    }

    void init(){
        const char* keyboard = "2";
        hangul_init();
        hic = hangul_ic_new(keyboard);
    }

    void final(){
        hangul_ic_delete(hic);
        hangul_fini();
    }

    HangulInputMethod *q_ptr;
    HangulInputContext *hic;
};

inline void commit(QObject *receiver, const QString &string)
{
    QInputMethodEvent inputEvent;
    inputEvent.setCommitString(string);
    QGuiApplication::sendEvent(receiver, &inputEvent);
}

inline void preedit(QObject *receiver, const QString &string = QString())
{
    QList<QInputMethodEvent::Attribute> attributes;
    QInputMethodEvent inputEvent(string, attributes);
    QGuiApplication::sendEvent(receiver, &inputEvent);
}

inline bool keyEventBypass(QObject *receiver,
                           QEvent::Type type,
                           Qt::Key key,
                           Qt::KeyboardModifier modifiers,
                           const QString& text = QString())
{
    QKeyEvent inputEvent(type, key, modifiers, text);
    return QGuiApplication::sendEvent(receiver, &inputEvent);
}

HangulInputMethod::HangulInputMethod(QObject *parent)
    : InputMethodBase(parent),
      d_ptr(new HangulInputMethodPrivate(this))
{

}

HangulInputMethod::~HangulInputMethod()
{
    Q_D(HangulInputMethod);

    d->final();
}

void HangulInputMethod::init()
{
    Q_D(HangulInputMethod);

    d->init();
}

bool HangulInputMethod::compose(const QChar &c)
{
    Q_D(HangulInputMethod);

    return hangul_ic_process(d->hic, c.toLatin1());
}

bool HangulInputMethod::decompose()
{
    Q_D(HangulInputMethod);

    return hangul_ic_backspace(d->hic);
}

void HangulInputMethod::keyEventHandler(QObject *receiver, Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers)
{
    Q_UNUSED(modifiers)

    if(receiver == nullptr)
        return;

    switch (key) {
    case Qt::Key_Backspace:
        if(decompose()){
            auto preeditString = this->preeditString();
            preedit(receiver, preeditString);
            return;
        }
        keyEventBypass(receiver, QEvent::KeyPress, key, Qt::NoModifier);
        return;
    case Qt::Key_Return: // fall-through
    case Qt::Key_Space:
        update(receiver);
        keyEventBypass(receiver, QEvent::KeyPress, key, Qt::NoModifier, text);
        return;
    default:
        if(text.isEmpty())
            return;

        if(compose(text.front())){
            // used to compose.
        }else{
            auto commitString = this->commitString();
            if(!commitString.isEmpty()){
                commit(receiver, commitString);
            }
            keyEventBypass(receiver, QEvent::KeyPress, key, Qt::NoModifier, text);
            return;
        }
    }

    auto commitString = this->commitString();
    auto preeditString = this->preeditString();

    if(!commitString.isEmpty()){
        commit(receiver, commitString);
    }

    if(!preeditString.isEmpty()){
        preedit(receiver, preeditString);
    }
}

QString HangulInputMethod::reset()
{
    Q_D(HangulInputMethod);

    return QString::fromUcs4(hangul_ic_flush(d->hic));
}

void HangulInputMethod::update(QObject *receiver)
{
    if(receiver == nullptr)
        return;

    auto preeditString = reset();

    if(!preeditString.isEmpty()){
        commit(receiver, preeditString);
    }else{
        preedit(receiver);
    }
}

QString HangulInputMethod::commitString() const
{
    Q_D(const HangulInputMethod);

    return QString::fromUcs4(hangul_ic_get_commit_string(d->hic));
}

QString HangulInputMethod::preeditString() const
{
    Q_D(const HangulInputMethod);

    return QString::fromUcs4(hangul_ic_get_preedit_string(d->hic));
}

#ifdef __cplusplus
}
#endif
