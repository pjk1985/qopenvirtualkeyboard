# QOpenVirtualkeyboard

*QOpenVirtualkeyboard* is a Qt5 VirtualKeyboard plugin library that provides korean, Latin and Number in your Qt applications.

## How To Build

There are currently supported build systems is qmake.

```
cd plugin
qmake
make
make install
```

## Roadmap

* provide a QML type for EGLFS

## License
QOpenVirtualkeyboard is released under the terms of the **GNU Lesser General Public License v3.0**. Full details in ``LICENSE`` file.

- libhangul is licensed under the LGPL v2.1
