import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.VirtualKeyboard 2.4

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    Rectangle{
        width: 100
        height: 50
        anchors.horizontalCenter: parent.horizontalCenter
        color: "transparent"
        border.width: 1

        TextEdit{
            anchors.fill: parent

        }
    }
}
